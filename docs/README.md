# Meltano Docs

## Prerequisites

- [NodeJS 8+](https://nodejs.org/en/)

## Getting Started

1. Navigate to project root directory in terminal
1. Run `npm install` or `yarn`
1. Run `npm run dev` or `yarn dev`

## Resources

- [VuePress](https://vuepress.vuejs.org)
